var express = require('express');
var router = express.Router();
var compiler = require('../compiler/compiler');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET home page. */
router.get('/compile', function(req, res, next) {


  res.json({ success: compiler.compile() });
});

module.exports = router;