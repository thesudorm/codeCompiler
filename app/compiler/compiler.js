var spawn = require('child_process').spawn;

function compile(){

    var output = "";

    var output = "test";

    // Run the compiler
    var compiler = spawn('g++', ['test.cpp']);

    // Event listeners
    // Trip during compilations
    compiler.stdout.on('data', function(data){
        output += 'stdout: ' + data;
        console.log(data);
    });

    compiler.stderr.on('data', function(data){
        output += String(data);
        console.log(data);
    });

    // Run the compiled program
    compiler.on('close', function(data){
        if (data === 0){
            var run = spawn('./a.out', []);

            run.stdout.on('data', function(result){
                output += String(result);
                console.log(result);
            });
            
            run.stderr.on('data', function(result){
                output += String(result);
                console.log(result);
            });

            run.on('close', function(result){
                output += 'stdout: ' + result;
                console.log('stdout: ' + result);
            });
        }

        // Remove the compiled program
        //var clean = spawn('rm', ['a.out]']);
    });


    console.log(output);

    return output;
}

module.exports = {
    compile: compile
};